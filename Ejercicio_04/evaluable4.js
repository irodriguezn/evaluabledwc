jugValencia=["Kempes","Botubot","Solsona","Arias", "Boro"];
jugBarcelona=["Quini","Iniesta","Messi","Ronaldo","Ronaldinho"];
jugMadrid=["Raúl","Mijatovic","Zidane","Butragueño", "Michel"];
jugMuseros=["Pelé","Maradona","Sócrates","Zico","Jordan"];
equipos=[jugValencia,jugMadrid,jugBarcelona,jugMuseros]

function rellenaEquipo(eq) {
    jugadores=equipos[eq]
    for (i=0;i<jugadores.length;i++) {
        opcion= new Option(jugadores[i], jugadores[i]);
        selJugadores.options[i+1]=opcion;
    }
}
    
function vaciaJugadores() {
    selJugadores.options.length=1;
}

function rellenaJugadores() {
    var selJugadores=document.getElementById("jugadores");
    var equipoSel;
    equipoSel=selEquipos.options[selEquipos.selectedIndex].value;
    switch (equipoSel) {
        case "Valencia":
            rellenaEquipo(0);
            break;
        case "Madrid":
            rellenaEquipo(1);
            break;
        case "Barcelona":
            rellenaEquipo(2);
            break;
        case "Museros":
            rellenaEquipo(3);        
            break;
        default:
            vaciaJugadores();
    }
  
   
}
window.onload = function() {
    selJugadores=document.getElementById("jugadores");    
    selEquipos=document.getElementById("equipos");
    selEquipos.addEventListener("change",rellenaJugadores, false);
}
