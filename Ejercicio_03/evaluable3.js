function convierteReal(cadena) {
    if (cadena.indexOf(",")!=-1) {
        cadena=cadena.replace(",",".");
    }
    if (isNaN(cadena)) {
        return Infinity;
    } else {
        return parseFloat(cadena);
    }
}

function espaciosHtml(num) {
    var aux="";
    for (i=1;i<=num;i++) {
        aux+="&nbsp;"
    }
    return aux;
}

function ajustaEspacios(cadena, num) {
    var aux=cadena;
    var l= cadena.length;
    if (l>=num) {
        return cadena.substring(0,num-1);
    } else {
        aux+=espaciosHtml(num-l);
        return aux;
    }
}
    
function anyade() {
    nombre=(txtNombre.value);
    peso=convierteReal(txtPeso.value);
    if (peso==Infinity) {
        alert("El peso no es válido!!");
        txtPeso.value="";
        txtPeso.focus();
        return "Error"
    }
    if (isNaN(txtAltura.value)) {
        alert("La altura no es válida. Compruebe que la ha dado en cm y sin decimales!!");
        txtAltura.value="";
        txtAltura.focus();
        return "Error"        
    } else {
        altura=parseInt(txtAltura.value);
        if (altura<20 || altura>300) {
            alert("La altura no es válida. Compruebe que la ha dado en cm y sin decimales!!");
            txtAltura.value="";
            txtAltura.focus();
            return "Error"        
        }    
    }
    
    long=selLista.length
    var lineas=[];
    if (long==1) {
        lineas[0]="<option value=''>Nombre" + espaciosHtml(9)+"Peso"+espaciosHtml(11)+"Altura"+espaciosHtml(11)+"</option>";
        if (peso>100) {
            lineas[1]="<option value='"+nombre+"' class='pAlto'>"+ajustaEspacios(nombre,15)+ajustaEspacios(peso.toString(),15)+ajustaEspacios(altura.toString(),15)+"</option>";
        } else {
            lineas[1]="<option value='"+nombre+"' class='pNormal'>"+ajustaEspacios(nombre,15)+ajustaEspacios(peso.toString(),15)+ajustaEspacios(altura.toString(),15)+"</option>";
        }
        selLista.innerHTML=lineas[0]+lineas[1];;
    } else {
        if (peso>100) {
            lineas[long]="<option value='"+nombre+"' class='pAlto'>"+ajustaEspacios(nombre,15)+ajustaEspacios(peso.toString(),15)+ajustaEspacios(altura.toString(),15)+"</option>";
        } else {
            lineas[long]="<option value='"+nombre+"' class='pNormal'>"+ajustaEspacios(nombre,15)+ajustaEspacios(peso.toString(),15)+ajustaEspacios(altura.toString(),15)+"</option>";
        }
        selLista.innerHTML=selLista.innerHTML+lineas[long];
    }
}
   
window.onload = function() {
    txtNombre=document.getElementById("nombre");
    txtPeso=document.getElementById("peso");    
    txtAltura=document.getElementById("altura");  
    btnAnyadir=document.getElementById("anyadir");
    btnAnyadir.addEventListener("click",anyade, false);
    selLista=document.getElementById("lista");
}
